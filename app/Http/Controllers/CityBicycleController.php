<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CityBicycleController extends Controller
{
    public function getStations(){
		$ch = curl_init("https://oslobysykkel.no/api/v1/stations"); 
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Client-Identifier: 429576e21341744b37171ec0f2524b72"));
        //curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($ch);
        
		//$info = curl_getinfo($ch);
        curl_close($ch);
    
        return response(stripslashes($data), 200);
    }

    public function getAvailability(){
        $ch = curl_init("https://oslobysykkel.no/api/v1/stations/availability"); 
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Client-Identifier: 429576e21341744b37171ec0f2524b72"));
        //curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($ch);
        
		//$info = curl_getinfo($ch);
        curl_close($ch);
    
        return response(stripslashes($data), 200);
        
    }
}
